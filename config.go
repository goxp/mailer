package mailer

// SmtpConfig ...
type SmtpConfig struct {
	Host      string `env:"SMTP_HOST"`
	User      string `env:"SMTP_USER"`
	Pass      string `env:"SMTP_PASS"`
	Port      int    `env:"SMTP_PORT"`
	Tls       bool   `env:"SMTP_TLS" envDefault:"false"`
	SendDelay int    `env:"SMTP_SEND_DELAY" envDefault:"100"`
}
