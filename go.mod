module gitlab.com/goxp/mailer

go 1.18

require (
	github.com/sirupsen/logrus v1.8.1
	gopkg.in/mail.v2 v2.3.1
)

require (
	golang.org/x/sys v0.0.0-20220412211240-33da011f77ad // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
)
