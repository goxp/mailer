package mailer

import (
	"crypto/tls"
	"io"
	"time"

	"github.com/sirupsen/logrus"
	"gopkg.in/mail.v2"
)

//go:generate mockery --name MailClient

// Email presents an email message
type Email struct {
	From         string
	To           map[string]string
	Subject      string
	HtmlMessage  string
	PlainMessage string
	MessageID    string
	Date         time.Time
	Attachments  map[string]io.Reader
}

// MailClient presents a smtp mail client
type MailClient interface {
	StartDaemon() error
	Send(email *Email)
	Stop()
}

type gomailClient struct {
	config *SmtpConfig

	messagesCh chan *mail.Message
	stopCh     chan struct{}
	log        *logrus.Entry
}

// Stop stop daemon
func (g *gomailClient) Stop() {
	close(g.stopCh)
}

func (g *gomailClient) buildMessage(param *Email) *mail.Message {
	m := mail.NewMessage()
	m.SetHeader("From", param.From)

	toAddresses := make([]string, len(param.To))
	i := 0
	for name, recipient := range param.To {
		toAddresses[i] = m.FormatAddress(recipient, name)
		i++
	}

	m.SetHeader("To", toAddresses...)
	m.SetHeader("Message-ID", param.MessageID)

	m.SetDateHeader("Date", param.Date)

	m.SetHeader("Subject", param.Subject)

	if len(param.PlainMessage) == 0 {
		m.SetBody("text/plain", "please view in a client that supports HTML")
	} else {
		m.SetBody("text/plain", param.PlainMessage)
	}

	// add html part
	if len(param.HtmlMessage) > 0 {
		m.AddAlternative("text/html", param.HtmlMessage)
	}

	// add attachments
	if len(param.Attachments) > 0 {
		for key, reader := range param.Attachments {
			m.AttachReader(key, reader)
		}
	}

	return m
}

func (g *gomailClient) makeDialer() *mail.Dialer {
	d := mail.NewDialer(g.config.Host, g.config.Port, g.config.User, g.config.Pass)
	if !g.config.Tls {
		d.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	}

	return d
}

// Send ...
func (g *gomailClient) Send(param *Email) {
	m := g.buildMessage(param)
	g.messagesCh <- m
}

// NewMailClient initialize a mail client, this also start the background process (daemon)
func NewMailClient(smtpConfig *SmtpConfig, log ...*logrus.Entry) MailClient {
	o := &gomailClient{
		config:     smtpConfig,
		messagesCh: make(chan *mail.Message, 1000),
		stopCh:     make(chan struct{}),
	}
	if len(log) == 0 {
		o.log = logrus.WithField("tag", "GoMail")
	} else {
		o.log = log[0]
	}

	return o
}

// StartDaemon starts dialing & the sending process
// this returns error when getting error while dialing
func (g *gomailClient) StartDaemon() error {
	d := g.makeDialer()
	d.Timeout = time.Second * 10

	var (
		tag = "[gomailClient.StartDaemon] "
		s   mail.SendCloser
		log = g.log
		err error
	)

	log.Info(tag + "mailer daemon starting ...")

	if s, err = d.Dial(); err != nil {
		return err
	}
	open := true

	go func() {
		for {
			select {
			case m, ok := <-g.messagesCh:
				if !ok {
					return
				}

				// wait a bit before send more
				if g.config.SendDelay > 10 {
					<-time.After(time.Duration(g.config.SendDelay) * time.Millisecond)
				}

				// reopen connection if closed
				if !open {
					if s, err = d.Dial(); err != nil {
						log.Errorf(tag+"failed to dial smtp server: %v", err)

						// push back for retry
						g.messagesCh <- m
						continue
					}
					open = true
				}

				if err = mail.Send(s, m); err != nil {
					log.Errorf(tag+"failed to send email: %v", err)
				} else {
					log.Debugf("email `%s` sent to `%s`", m.GetHeader("Subject"), m.GetHeader("To"))
				}

			// close the connection to the SMTP server if no email was sent in the lats 30 seconds
			case <-time.After(30 * time.Second):
				if open {
					_ = s.Close()
					open = false
				}

			// stop signal
			case <-g.stopCh:
				if open {
					_ = s.Close()
				}
				return
			}
		}
	}()

	return nil
}
